var data = [{
    "name": "H&M",
    "description": "Молодежная одежда",
    "categoryId": "1",
    "room": "101"
}, {
    "name": "Стокманн",
    "description": "Элитная одежда",
    "categoryId": "1",
    "room": "202"
}, {
    "name": "INCANTO",
    "description": "Женская одежда",
    "categoryId": "1",
    "room": "112"
}, {
    "categoryId": "291",
    "emal": null,
    "id": "44966",
    "name": "Monica Ricci",
    "picture": "iblock/9ff/monica-ricci.jpg",
    "room": "102",
    "tel": "379-51-13",
    "text": "Магазин женской одежды Monica Ricci представляет одноименный итальянский бренд Магазин женской одежды Monica Ricci представляет одноименный итальянский бренд Магазин женской одежды Monica Ricci представляет одноименный итальянский бренд Магазин женской одежды Monica Ricci представляет одноименный итальянский бренд Магазин женской одежды Monica Ricci представляет одноименный итальянский бренд Магазин женской одежды Monica Ricci представляет одноименный итальянский бренд",
    "www": "www.monicaricci.it"
}]

var categories = {
    "291": { name: "Женское", color: "#99b3ff" },
    "292": { name: "Мужское", color: "#ff6699" },
    "293": { name: "Детские", color: "#ffff99" },
    "294": { name: "Обувь", color: "#00ff00" },
    "295": { name: "Сумки", color: "#ff9900" },
    "296": { name: "Аксессуары", color: "#009933" },
    "297": { name: "Спорттовары", color: "#660066" },
    "298": { name: "Товары для дома", color: "#003399" },
    "299": { name: "Парфюмерия", color: "#993366" },
    "626": { name: "Подарки", color: "#00ff99" },
    "301": { name: "Белье", color: "#00ff99" },
    "302": { name: "Часы", color: "#00ff99" },
    "303": { name: "Ювелирные", color: "#00ff99" },
    "304": { name: "Электроника", color: "#00ff99" }
}

var app = (function() {

    var room, index, _data;

    var $stores = $(".store");
    var $descs = $(".floor .desc");
    var $popup = $("#popup");
    var $popupContainer = $("#popup-container");
    var $popupCloseBtn = $("#popup-close");
    var $floors = $(".floor");
    var $controls = $(".control");

    var popupTpl = _.template($("#popup-template").text());
    var storeTpl = _.template($("#store-template").text());

    function loadData() {
        $.ajax({
            url: 'post.php',
            success: fillStores,
            error: function() {
                console.log('error')
            },
            complete: function() {
                console.log('complete')
            }
        });
    }

    // function fillStores(data) {
    function fillStores() {
        console.log('success', data)
        _data = data;

        $popup.hide();

        $descs.each(function(i, e) {
            room = $(e).attr('id').split('_')[1];

            index = _.findIndex(_data, { room: room })

            if (index > -1) {
                $("#room_" + room).css('fill', categories[_data[index].categoryId].color);
                $(e).append(storeTpl({ obj: _data[index] }));
            }
        })
    }

    function storesClickHandler() {
        $popup.hide();
        $popupContainer.empty();

        room = $(this).attr('id').split('_')[1];
        index = _.findIndex(_data, { room: room })
        if (index > -1) {
            $popupContainer.append(popupTpl({ obj: _data[index] }));
            $popup.show();
            return;
        }
    }

    function popupCloseBtnClickHandler() {
        $popup.hide();
        $popupContainer.empty();
    }

    function controlsClickHandler(e) {
        $floors.removeClass("active")
        $controls.removeClass("active")

        $(this).addClass('active')
        $("#floor" + $(this).attr('id').split("-")[2]).addClass("active")
    }

    return {
        init: function() {
            // loadData();
            fillStores();
            $stores.on('click', storesClickHandler)
            $popupCloseBtn.on('click', popupCloseBtnClickHandler)
            $controls.on("click", controlsClickHandler);

            $('#floor1').addClass('active')
            $("#j-control-1").addClass("active")
            $("#popup").hide();
        }
    }
})();

app.init();
