var gulp = require('gulp')
var tasks = require('./gulp/')

/* @gulp: default */
gulp.task('default', ['dev'])

/* @gulp: dist */
gulp.task('dist', ['bower', 'assets', 'js', 'style', 'view'])

/* @gulp: dev */
gulp.task('dev', ['dist', 'watch'], tasks.BSync)

/* @gulp: javascript */
// gulp.task('js', ['docs'], tasks.Js)
gulp.task('js', tasks.Js)

/* @gulp: bower */
gulp.task('bower', tasks.Bower)

/* @gulp: assets */
gulp.task('assets', tasks.Assets)

/* @gulp: view */
gulp.task('view', tasks.View)

/* @gulp: styles */
gulp.task('style', tasks.Style)

/* @gulp: watch */
gulp.task('watch', tasks.Watch)

/* @gulp: docs */
//gulp.task('docs', tasks.Docs)
