/* def: path constants */
module.exports.PROJECT_NAME = 'vigenere-cipher'
module.exports.DIST_PATH = 'dist'
module.exports.DOCS_PATH = 'doc'

/* def: tasks entries */
module.exports.ENTRIES = {
    js: 'src/app/bootstrap.js',
    assets: 'src/assets/**/*',
    view: 'src/app/index.jade',
    style: 'src/app/style.styl'
}

/* def: watch entries */
module.exports.WATCHERS = {
    assets: 'src/assets/**/*',
    view: 'src/app/**/*.jade',
    style: 'src/app/**/*.styl',
    js: 'src/app/**/*.js'
}

/* def: environment constants */
const ENV = process.env.NODE_ENV
module.exports.ENV = ENV
module.exports.WATCH = (ENV !== 'production')
module.exports.DEV = ENV !== 'production'