var gulp = require('gulp')
var path = require('path')
var gulpif = require('gulp-if')
var uglify = require('gulp-uglify')
var jsdoc = require('gulp-jsdoc')
var plumber = require('gulp-plumber')
var notify = require('gulp-notify')
var bs = require('browser-sync')

var consts = require('../const')

module.exports.default = function() {
    gulp.src(consts.WATCHERS.js)
        .pipe(plumber())
        .pipe(jsdoc(path.join(consts.DOCS_PATH, 'doc')))
        .pipe(notify({ message: 'docs complete' }))
        .pipe(bs.reload({
            stream: true
        }))
}
