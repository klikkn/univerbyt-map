var gulp = require('gulp')
var notify = require('gulp-notify')
var bs = require('browser-sync')
var jade = require('gulp-jade')

var consts = require('../const')

module.exports.default = function() {
    return gulp.src(consts.ENTRIES.view)
    	.pipe(jade())
        .pipe(gulp.dest(consts.DIST_PATH))
        .pipe(notify({ message: 'view complete' }))
        .pipe(bs.reload({
            stream: true
        }))
}
