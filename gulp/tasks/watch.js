var gulp = require('gulp')

var consts = require('../const')

module.exports.default = function() {
    gulp.watch(consts.WATCHERS.view, ['view'])
    gulp.watch(consts.WATCHERS.assets, ['assets'])
    gulp.watch(consts.WATCHERS.style, ['style'])
    gulp.watch(consts.WATCHERS.js, ['js'])
}
