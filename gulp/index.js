module.exports.Assets = require('./tasks/assets').default
module.exports.Bower = require('./tasks/bower').default
module.exports.BSync = require('./tasks/browser-sync').default
module.exports.Js = require('./tasks/js').default
module.exports.Style = require('./tasks/style').default
module.exports.View = require('./tasks/view').default
module.exports.Watch = require('./tasks/watch').default
//module.exports.Docs = require('./tasks/docs').default
